{ pkgs ? import <nixpkgs> { } }:
let
  taskfiles = (pkgs.rustPlatform.buildRustPackage rec {
    pname = "taskfiles";
    version = "0.1.0";
    cargoLock.lockFile = ./Cargo.lock;
    src = pkgs.lib.cleanSource ./.;
  }).overrideAttrs (oldAttrs: {
    buildInputs = (oldAttrs.buildInputs or []) ++ [

      # Specify runtime dependencies here.
      # And add them to PATH in the script below.
      pkgs.bashInteractive
      pkgs.fzf
      pkgs.glow

    ];
  });
in

# As far as I can tell, wrapping the package in a script is the only way to get runtime dependencies on PATH.
# Taskfiles runs these with a `std::process::Command` and they weren't getting picked up by nix's automatic
# runtime dependency detection (https://nixos.org/guides/nix-pills/automatic-runtime-dependencies#id1401).
# This is my first nix package though so maybe (hopefully) I'm doing something wrong!
# This got me most of the way there: https://stackoverflow.com/a/68330101
(pkgs.writeScriptBin "taskfiles" ''
  #!${pkgs.stdenv.shell}

  # Update PATH for all runtime dependencies listed in buildInputs,
  export PATH=${pkgs.glow}/bin:$PATH
  export PATH=${pkgs.fzf}/bin:$PATH
  export PATH=${pkgs.bashInteractive}/bin:$PATH

  exec ${taskfiles}/bin/taskfiles "$@"
''
)
