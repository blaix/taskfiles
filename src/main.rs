use chrono::Datelike;
use chrono::Duration;
use clap::{Parser, Subcommand};
use glob::glob;
use rand::seq::SliceRandom;
use serde::Deserialize;
use std::cmp::Ordering;
use std::env;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::time;
use toml;
use walkdir::WalkDir;

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    command: Option<SubCommand>,
}

#[derive(Debug, Subcommand)]
enum SubCommand {
    // --------------------------------------------------------------
    // TODO: Change default to a new prompt for what to do with file:
    //       Open (old default behavior), Mark Done, Delete, Cancel.
    // --------------------------------------------------------------
    /// Choose task from prioritized list (default behavior).
    Next,
    /// Choose task from randomly ordered list.
    Random,
    /// Choose task from list sorted by available date, oldest first.
    Oldest,
    /// Create new task (or open if exists). Prompts for directory and copies _template.md if it exists.
    New,
    /// Like "new", but uses today's date as the taskfile name.
    Daily,
    /// Like "new", but uses last monday's date as the taskfile name.
    Weekly,
    /// Like "new", but uses tomorrow's date as the taskfile name.
    Tomorrow,
    /// Like "new", but uses next monday's date as the taskfile name.
    Monday,
    /// Like "new", but uses first of the month as the taskfile name.
    Monthly,
    /// Mark a taskfile as done (by moving it to a _done folder).
    Done,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct Task {
    // List fields in order of priority for sorting
    priority: u8,
    due: String,
    scheduled: String,
    available: String,
    name: String,
}

struct Env {
    home: String,
    editor: String,
}

const EXT: &str = ".md";
const HOME_VAR: &str = "TASKFILES_HOME";
const EDITOR_VAR: &str = "EDITOR";
const DATE_FORMAT: &str = "%F";
const FZF_OPTS: &str = "\
    --ansi \
    --keep-right \
    --layout=reverse \
    --info=hidden \
    --border \
    --border-label-pos=5:top \
";

fn main() {
    let args = Args::parse();

    let env = Env {
        home: env::var(&HOME_VAR).expect(&format!("${HOME_VAR} is not set.")),
        editor: env::var(&EDITOR_VAR).expect(&format!("${EDITOR_VAR} is not set.")),
    };

    let choice = match &args.command {
        Some(SubCommand::New)
        | Some(SubCommand::Daily)
        | Some(SubCommand::Weekly)
        | Some(SubCommand::Monday)
        | Some(SubCommand::Monthly)
        | Some(SubCommand::Tomorrow) => create_task(&env, &args.command),
        subcommand => choose_task(subcommand, &env),
    };

    match choice {
        None => (),
        Some(taskfile) => {
            let taskfile = if !taskfile.ends_with(&EXT) {
                taskfile + EXT
            } else {
                taskfile
            };
            match &args.command {
                Some(SubCommand::Done) => complete_task(&env, &taskfile),
                _ => open_task(&env, &taskfile),
            };
        }
    }
}

fn open_task(env: &Env, taskfile: &str) {
    let open_taskfile = format!("cd \"{}\" && {} \"{taskfile}\"", env.home, env.editor);
    Command::new("/bin/sh")
        .args(["-c", &open_taskfile])
        .spawn()
        .expect("Failed to open task")
        .wait()
        .unwrap();
}

fn complete_task(env: &Env, taskfile: &str) {
    let path = PathBuf::from(format!("{}/{taskfile}", env.home));
    let file_name = path.file_name().unwrap();

    let mut done_dir = path.parent().unwrap().to_path_buf();
    done_dir.push("_done");

    let mut destination = done_dir.clone();
    destination.push(&file_name);

    if !done_dir.exists() {
        fs::create_dir(&done_dir).unwrap();
    }

    fs::rename(&path, &destination).expect("Error moving task to _done");
    println!("Moved to {:?}", &destination);
}

fn today() -> String {
    chrono::Local::now().format(&DATE_FORMAT).to_string()
}

fn tomorrow() -> String {
    chrono::Local::now()
        .checked_add_signed(Duration::days(1))
        .expect("Failed to calculate tomorrow oh no!")
        .format(&DATE_FORMAT)
        .to_string()
}

fn monday() -> String {
    let now = chrono::Local::now();
    let weekday = now.date_naive().weekday();
    let days_since_monday = weekday.num_days_from_monday();
    now.checked_sub_signed(Duration::days(days_since_monday.into()))
        .expect("Failed to find monday oh no!")
        .format(&DATE_FORMAT)
        .to_string()
}

#[test]
fn test_monday() {
    // TODO: refactor to test range of dates instead of literal today
    let monday = monday();
    println!("Monday is: {}", &monday);
    let weekday = chrono::NaiveDate::parse_from_str(&monday, &DATE_FORMAT)
        .unwrap()
        .weekday();
    assert!(weekday == chrono::Weekday::Mon);
}

fn next_monday() -> String {
    let now = chrono::Local::now();
    let weekday = now.date_naive().weekday();
    let days_since_monday = weekday.num_days_from_monday();
    let days_until_monday = match days_since_monday {
        7 => 0,
        n => 7 - n,
    };
    now.checked_add_signed(Duration::days(days_until_monday.into()))
        .expect("Failed to find monday oh no!")
        .format(&DATE_FORMAT)
        .to_string()
}

#[test]
fn test_next_monday() {
    // TODO: refactor to test range of dates instead of literal today
    let monday = next_monday();
    println!("Next monday is: {}", &monday);
    let weekday = chrono::NaiveDate::parse_from_str(&monday, &DATE_FORMAT)
        .unwrap()
        .weekday();
    assert!(weekday == chrono::Weekday::Mon);
}

fn first_of_month() -> String {
    chrono::Local::now()
        .with_day(1)
        .expect("Failed to get first of month")
        .format(&DATE_FORMAT)
        .to_string()
}

#[test]
fn test_first_of_month() {
    // TODO: refactor to test range of dates instead of literal today
    let now = chrono::Local::now();
    let first = first_of_month();
    let parsed = chrono::NaiveDate::parse_from_str(&first, &DATE_FORMAT).unwrap();
    println!("First of month is: {}", &first);
    assert!(parsed.day() == 1);
    assert!(parsed.month() == now.month());
    assert!(parsed.year() == now.year());
}

fn create_task(env: &Env, cmd: &Option<SubCommand>) -> Option<String> {
    let dir = choose_dir(&env);
    let today = today();
    let tomorrow = tomorrow();
    let monday = monday();
    let next_monday = next_monday();
    let first_of_month = first_of_month();
    match dir {
        None => None,
        Some(dir) => {
            let dir = Path::new(&env.home).join(dir);
            let (task_name, date) = match cmd {
                Some(SubCommand::New) => {
                    let readline = Command::new("sh")
                        .args([
                            "-c",
                            &format!(
                                "read -re -i \"{}/\" -p \"Filename: \" input && echo \"$input\"",
                                dir.display()
                            ),
                        ])
                        .stdout(Stdio::piped())
                        .spawn()
                        .expect("Failed to call read for filename");
                    let output = readline
                        .wait_with_output()
                        .expect("Can't get readline output");
                    let stdout =
                        String::from_utf8(output.stdout).expect("Can't convert output to string");
                    let filename = stdout.trim().to_string();
                    (filename, today.clone())
                }
                Some(SubCommand::Daily) => (today.clone(), today.clone()),
                Some(SubCommand::Weekly) => (monday.clone(), monday.clone()),
                Some(SubCommand::Monday) => (next_monday.clone(), next_monday.clone()),
                Some(SubCommand::Tomorrow) => (tomorrow.clone(), tomorrow.clone()),
                Some(SubCommand::Monthly) => (first_of_month.clone(), first_of_month.clone()),
                oops => {
                    panic!(
                        "{:?} is not a valid subcommand for creating a new task.",
                        oops
                    );
                }
            };
            if task_name == "".to_string() {
                None
            } else {
                let task_name = if task_name.ends_with(&EXT) {
                    task_name.clone()
                } else {
                    format!("{}{}", &task_name, &EXT)
                }
                .replace("\\ ", " "); // Remove carry-overs from readline tab completion
                let task = dir.join(&task_name);
                use_template_if_exists(&dir, &task, &date);
                Some(task.to_string_lossy().to_string())
            }
        }
    }
}

fn use_template_if_exists(dir: &PathBuf, task: &PathBuf, date: &String) {
    let template = dir.join("_template.md");
    let template_path = template.display().to_string();
    let task_path = task.display().to_string();

    if template.exists() && !task.exists() {
        let contents = fs::read_to_string(&template)
            .expect(&format!("Can't read template file: {}", &template_path));
        let contents = contents.replace("%%DATE%%", &date);
        let mut file =
            File::create(&task_path).expect(&format!("Can't create task file: {}", &task_path));
        file.write_all(contents.as_bytes()).expect(&format!(
            "Can't write template contents to file: {}",
            &task_path
        ));
    }
}

fn choose_dir(env: &Env) -> Option<String> {
    let fzf_opts = "--preview 'ls {}' \
        --border-label='Start typing or use arrow keys to select directory'";
    let mut fzf = fzf(&env, &fzf_opts);
    for e in WalkDir::new(&env.home) {
        if let Ok(entry) = e {
            if entry.file_type().is_dir() {
                let dir = entry.path().display().to_string();
                let dir = strip_prefix(&env, &dir);
                if dir != ""
                    && !dir.starts_with(".")
                    && !dir.contains("/.")
                    && !dir.ends_with("/_done")
                    && !dir.contains("/_done/")
                {
                    write!(fzf.stdin.as_mut().unwrap(), "{}\n", dir).unwrap();
                }
            }
        }
    }
    fzf_choice(fzf)
}

fn choose_task(command: &Option<SubCommand>, env: &Env) -> Option<String> {
    let pattern = format!("{}/**/*{}", &env.home, &EXT);
    let files = glob(&pattern).expect(&format!("Bad file pattern: {}", &pattern));
    let mut tasks: Vec<Task> = vec![];

    let mut _num_tasks = 0;
    let _start = time::Instant::now();
    for file in files {
        _num_tasks += 1;
        match file {
            Err(e) => panic!("Error reading file: {}", e),
            Ok(path) => {
                if let Some(task) = to_task(&path) {
                    tasks.push(task);
                }
            }
        }
    }
    let _duration = _start.elapsed();
    println!("Time to parse {} tasks: {:?}", _num_tasks, _duration);

    match command {
        Some(SubCommand::Random) => {
            let mut rng = rand::thread_rng();
            tasks.shuffle(&mut rng);
        }
        Some(SubCommand::Oldest) => {
            tasks.sort_by(|a, b| a.available.cmp(&b.available));
        }
        _ => tasks.sort(),
    }

    let fzf_opts = format!(
        "--no-sort \
        --tiebreak=index \
        --preview 'echo {{}}{EXT} && glow --style=dracula {{}}{EXT}' \
        --border-label='Start typing or use arrow keys to select task' \
        "
    );

    let mut fzf = fzf(&env, &fzf_opts);

    for task in tasks {
        let without_prefix = strip_prefix(&env, &task.name);
        let display_name = without_prefix.trim_end_matches(&EXT);
        write!(fzf.stdin.as_mut().unwrap(), "{}\n", display_name).unwrap();
    }

    fzf_choice(fzf)
}

#[derive(Debug, Deserialize)]
struct Frontmatter {
    taskfile: TaskfileFrontmatter,
}

#[derive(Debug, Deserialize)]
struct TaskfileFrontmatter {
    priority: Option<u8>,
    due: Option<String>,
    scheduled: Option<String>,
    available: Option<String>,
}

fn to_task(path: &PathBuf) -> Option<Task> {
    let name = path.display().to_string();
    if name.contains("/_") || name.contains("/.") {
        return None;
    }

    let lowest_priority = 9;
    let lowest_date = "zzzz-zz-zz".to_string();

    let contents = fs::read_to_string(path).expect(&format!("Can't read file: {}", path.display()));
    let frontmatter = match extract_frontmatter(&contents) {
        None => Frontmatter {
            taskfile: TaskfileFrontmatter {
                priority: None,
                due: None,
                scheduled: None,
                available: None,
            },
        },
        Some(fm) => toml::from_str(&fm).expect(&format!(
            "Failed to parse frontmatter: {}\nin: {}",
            &fm, &name
        )),
    };

    let priority = match frontmatter.taskfile.priority {
        Some(priority) => priority,
        None => lowest_priority,
    };

    let due = match frontmatter.taskfile.due {
        Some(due) if due != "".to_string() => due,
        _ => lowest_date.clone(),
    };

    let scheduled = match frontmatter.taskfile.scheduled {
        Some(scheduled) if scheduled != "".to_string() => scheduled,
        _ => lowest_date.clone(),
    };

    let available = match frontmatter.taskfile.available {
        Some(available) if available != "".to_string() => available,
        _ => "0001-01-01".to_string(),
    };

    match available.cmp(&today()) {
        Ordering::Greater => None,
        _ => Some(Task {
            name,
            priority,
            due,
            scheduled,
            available,
        }),
    }
}

fn fzf(env: &Env, options: &str) -> std::process::Child {
    Command::new("/bin/sh")
        .args([
            "-c",
            &format!("cd {} && fzf {FZF_OPTS} {options}", env.home,),
        ])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to run fzf!")
}

fn fzf_choice(fzf: std::process::Child) -> Option<String> {
    let output = fzf.wait_with_output().expect("Can't get fzf output");
    let stdout = String::from_utf8(output.stdout).expect("Can't convert fzf output to string");
    let choice = stdout.trim().to_string();
    if choice == "" {
        None
    } else {
        Some(choice)
    }
}

fn strip_prefix(env: &Env, path: &str) -> String {
    path.trim_start_matches(&env.home)
        .trim_start_matches("/")
        .trim_start_matches("\\")
        .to_string()
}

fn extract_frontmatter(text: &str) -> Option<&str> {
    if text.starts_with("+++\n") {
        let fm_start = 4;
        match &text[4..].find("+++\n") {
            None => None,
            Some(end) => {
                let fm_end = end + 4;
                Some(&text[fm_start..fm_end].trim())
            }
        }
    } else {
        None
    }
}

#[test]
fn test_extract_frontmatter() {
    let test_string = "+++\nsome frontmatter\n+++\nthe rest";
    println!("Original String{:?}", test_string);
    let matter = extract_frontmatter(test_string);
    println!("Matter: {:?}", matter);
    assert!(matter == Some("some frontmatter"));
}

#[test]
fn test_extract_missing_frontmatter() {
    let test_string = "nope";
    println!("Original String{:?}", test_string);
    let matter = extract_frontmatter(test_string);
    println!("Matter: {:?}", matter);
    assert!(matter == None);
}

#[test]
fn test_extract_invalid_frontmatter() {
    let test_string = "+++\nsome frontmatter?\noops!";
    println!("Original String{:?}", test_string);
    let matter = extract_frontmatter(test_string);
    println!("Matter: {:?}", matter);
    assert!(matter == None);
}
