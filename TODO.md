* [X] init rust project
* [X] Use nix rust overlay: https://codeberg.org/blaix/public-wiki/wiki/Nix-snippets#rust
* [X] See this 30 min guide to rust: https://fasterthanli.me/articles/a-half-hour-to-learn-rust
* [X] binary to get sorted task list (synchronously)
* [X] Migrate from "tasker" to this project.
* [X] Parse toml frontmatter instead of %[...] tokens
* [X] Add "tomorrow" subcommand
* [ ] Try a rewrite in gren:
  * [ ] Proof of concept speed test compared to rust
